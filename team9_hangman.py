import random
from tkinter import *
from tkinter import messagebox
import pygame
from tkinter import PhotoImage

pygame.mixer.init()
correct_sound = pygame.mixer.Sound(r'correct.mp3')
wrong_sound = pygame.mixer.Sound(r'wrong.mp3')
background_sound = pygame.mixer.Sound(r'background.mp3')
background_sound.play(-1)
win_sound = pygame.mixer.Sound(r'win.wav')
lose_sound = pygame.mixer.Sound(r'lose.wav')
score = 0
run = True
green = (0, 255, 0)
blue = (0, 0, 100)


def start_game():
    global run
    run = True
    # root.destroy()
    if root:
        root.destroy()
    root()


def exit_game():
    global run
    answer = messagebox.askyesno('ALERT', 'YOU WANT TO EXIT THE GAME?')
    if answer:
        run = False
        root.destroy()


def exit_game_and_destroy():
    global root_result, root
    if root_result:
        root_result.destroy()
    if root:
        root.destroy
    exit()


def restart_game():
    global score, run, root_result, root
    # score = 0
    run = False

    if root_result:
        root_result.destroy()
    if root:
        root.destroy()
    if win_count != len(selected_word):
        score = 0
    background_sound.play(-1)

    start_game()


def show_result(win_count, count):
    global root_result
    root_result = Toplevel(root)
    root_result.geometry('905x800')
    root_result.config(bg="#A9A9A9")
    if win_count == len(selected_word):
        root_result_label = Label(root_result, text="Congratulations! \n You have WON the game", font=("Helvetica", 32))
        root_result_label.place(x=200, y=10)
        winner_image = PhotoImage(file="winner1.png")
        image_label = Label(root_result, image=winner_image)
        image_label.place(x=280, y=180)
        background_sound.stop()
        win_sound.play()


    elif count == 6:
        root_result_label1 = Label(root_result, text="You have lost the game.\nBetter luck next time!",
                                   font=("Helvetica", 32))
        root_result_label1.place(x=210, y=10)
        loose_image = PhotoImage(file="loose1.png")
        image_label = Label(root_result, image=loose_image)
        image_label.place(x=280, y=155)
        correct_word_label = Label(root_result, text=f"The correct word was: {selected_word}", font=("Arial", 24))
        correct_word_label.place(x=240, y=400)
        background_sound.stop()
        lose_sound.play()

    play_again_label = Label(root_result, text="Do you want to play again?", font=("Helvetica", 32))
    play_again_label.place(x=200, y=450)

    yes_button = Button(root_result, text="Yes", font=("Arial", 32), background="#32CD32", command=restart_game)
    yes_button.place(x=300, y=550)

    no_button = Button(root_result, text="No", font=("Arial", 32), background="red", command=exit_game_and_destroy)
    no_button.place(x=500, y=550)
    # root.destroy()
    root_result.mainloop()

    # root.destroy()
    # root_result.mainloop()


# put this in a function
'''def show_instructions():
    instructions = """
    Welcome to Hangman Game!
    How to Play:
    1. A random word will be selected.
    2. Click on the letter buttons to guess the word.
    3. Each incorrect guess reveals parts of the hangman.
    4. Guess the word before the hangman is fully revealed.
    5. Your score increases with each correct guess.

    Good luck and have fun!
    """
    messagebox.showinfo("Instructions", instructions)'''

root = Tk()
root.geometry('800x600')
root.title('Hangman Game')
root.config(bg="black")
# welcome_label = Label(root, text="Welcome to Hangman Game", font=("Arial", 24))
# welcome_label.pack(pady=20)
# image=PhotoImage(file="logo.png")
# image_label=Label(root,image=image)
# image_label.place(x=280,y=50)
bg_image = PhotoImage(file="logo.png")
bg_label = Label(root, image=bg_image)
bg_label.place(relwidth=1, relheight=1)

start_button = Button(root, text="Start Game", font=("Arial", 32), background="#32CD32", command=start_game)
# start_button.pack()
start_button.place(x=570, y=590)

root.mainloop()

root = Tk()
root.geometry('800x600')
root.title('Hangman Game')
root.config(bg="#D8BFD8")
# instructions_button = Button(root, text="Instructions", font=("Arial", 16), command=show_instructions)
# instructions_button.pack()
lbl = Label(root, text="INSTRUCTIONS", fg='#800000', font=("Helvetica", 32), background="#D8BFD8")
lbl.place(x=200, y=0)
lb2 = Label(root,
            text="1. There is an array has a collection of words and \n game will choose one of these words by random way.",
            fg='black', font=("Helvetica", 24), background="#D8BFD8")
lb2.place(x=30, y=60)
lbl3 = Label(root,
             text="2. Then it will be shown on the screen like this (_ _ _ ) \n  according to the number of the letters of the word",
             fg='black', font=("Helvetica", 24), background="#D8BFD8")
lbl3.place(x=30, y=135)
lbl4 = Label(root, text="3. The player should guess this word what will be ?", fg='black', font=("Helvetica", 24),
             background="#D8BFD8")
lbl4.place(x=30, y=210)
lbl5 = Label(root, text="4. Then you should enter the first letter of the word \n which you guessed.", fg='black',
             font=("Helvetica", 24), background="#D8BFD8")
lbl5.place(x=30, y=248)
lbl6 = Label(root, text="5. If this letter is right it will be displayed on the screen.", fg='black',
             font=("Helvetica", 24), background="#D8BFD8")
lbl6.place(x=30, y=286)
lbl7 = Label(root,
             text="6. If you enter 6 letters wrong you will lose but if \n  you enter the right letters you will win.",
             fg='black', bg=root["bg"], font=("Helvetica", 24), background="#D8BFD8")
lbl7.place(x=30, y=324)
next_button = Button(root, text="NEXT", font=("Arial", 32), background="#32CD32", command=start_game)
next_button.place(x=180, y=500)
# next_button.pack()

exit_button = Button(root, text="EXIT", font=("Arial", 32), background="red", command=exit_game)
exit_button.place(x=500, y=500)
# exit_button.pack()

root.mainloop()

while run:
    root = Tk()
    root.geometry('905x800')
    root.title('HANGMAN')
    background_image = PhotoImage(file="marvel1.png")
    canvas = Canvas(root, width=900, height=700)
    canvas.pack()
    canvas.create_image(0, 0, imag=background_image, anchor=NW)
    root.config(bg='#E7FFFF')
    # root.attributes('-transparentcolor','#E7FFFF')
    x_start, y_start = 0, 705
    x_end, y_end = 905, 592

    canvas = Canvas()
    canvas.pack(fill=BOTH, expand=True)
    canvas.create_rectangle(x_start, y_start, x_end, y_end, fill='#E7FFFF')
    # canvas.create_rectangle(0,592,905,0,fill='#D2B48C')
    count = 0
    win_count = 0
    hangman_counter = 0
    index = random.randint(0, 3)
    file = open('words.txt', 'r')
    l = file.readlines()
    selected_word = l[index].strip('\n')
    score_label = Label(root, text=f"Score: {score}", font=("Arial", 20), bg="#E7FFFF")
    score_label.place(x=10, y=10)
    score_label = Label(root, text=f"Score: {score}", font=("Arial", 20), bg="#E7FFFF")
    score_label.place(x=10, y=10)

    x = 90
    for i in range(0, len(selected_word)):
        x += 60
        exec('d{}=Label(root,text="_",bg="#E7FFFF",font=("arial",40))'.format(i))
        exec('d{}.place(x={},y={})'.format(i, x, 450))
    al = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
          'w', 'x', 'y', 'z']
    for let in al:
        exec('{}=PhotoImage(file="{}.png")'.format(let, let))
    h123 = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'h7']
    for hangman in h123:
        exec('{}=PhotoImage(file="{}.png")'.format(hangman, hangman))
    button = [['b1', 'a', 0, 595], ['b2', 'b', 70, 595], ['b3', 'c', 140, 595], ['b4', 'd', 210, 595],
              ['b5', 'e', 280, 595], ['b6', 'f', 350, 595], ['b7', 'g', 420, 595], ['b8', 'h', 490, 595],
              ['b9', 'i', 560, 595], ['b10', 'j', 630, 595], ['b11', 'k', 700, 595], ['b12', 'l', 770, 595],
              ['b13', 'm', 840, 595], ['b14', 'n', 0, 645], ['b15', 'o', 70, 645], ['b16', 'p', 140, 645],
              ['b17', 'q', 210, 645], ['b18', 'r', 280, 645], ['b19', 's', 350, 645], ['b20', 't', 420, 645],
              ['b21', 'u', 490, 645], ['b22', 'v', 560, 645], ['b23', 'w', 630, 645], ['b24', 'x', 700, 645],
              ['b25', 'y', 770, 645], ['b26', 'z', 840, 645]]
    for q1 in button:
        exec(
            '{}=Button(root,bd=0,command=lambda:check("{}","{}"),bg="#E7FFFF",activebackground="#E7FFFF",font=10,image={})'.format(
                q1[0], q1[1], q1[0], q1[1]))
        exec('{}.place(x={},y={})'.format(q1[0], q1[2], q1[3]))
    han = [['c1', 'h1'], ['c2', 'h2'], ['c3', 'h3'], ['c4', 'h4'], ['c5', 'h5'], ['c6', 'h6'], ['c7', 'h7']]
    for p1 in han:
        exec('{}=Label(root,bg="#E7FFFF",image={})'.format(p1[0], p1[1]))

    c1.place(x=300, y=50)

    '''def close():
        global run
        answer = messagebox.askyesno('ALERT', 'YOU WANT TO EXIT THE GAME?')
        if answer == True:
            run = False
            root.destroy()


    e1 = PhotoImage(file='exit.png')
    ex = Button(root, bd=0, command=close, bg="#E7FFFF", activebackground="#E7FFFF", font=10, image=e1)
    ex.place(x=770, y=10)
    s2 = 'SCORE:' + str(score)
    s1 = Label(root, text=s2, bg="#E7FFFF", font=("arial", 25))
    s1.place(x=10, y=10)'''


    def check(letter, button):
        global count, win_count, run, score
        exec('{}.destroy()'.format(button))
        if letter in selected_word:
            for i in range(0, len(selected_word)):
                if selected_word[i] == letter:
                    correct_sound.play()
                    win_count += 1
                    exec('d{}.config(text="{}")'.format(i, letter.upper()))
            if win_count == len(selected_word):
                score += 1
                score_label.config(text=f"Score: {score}")  # Update the score label
                show_result(win_count, count)

                # result_text = ('GAME OVER', 'YOU WON')
                show_result(win_count, count)
                '''answer = messagebox.askyesno('GAME OVER', 'YOU WON!\nWANT TO PLAY AGAIN?')
                if answer == True:
                    run = True
                    root.destroy()
                else:`
                    run = False
                    root.destroy()'''
        else:
            wrong_sound.play()
            count += 1
            exec('c{}.destroy()'.format(count))
            exec('c{}.place(x={},y={})'.format(count + 1, 300, 50))
            if count == 6:
                correct_word_label = Label(root, text=f"The correct word was: {selected_word}", font=("Arial", 32),
                                           bg="#D2B48C")
                correct_word_label.place(x=130, y=530)
                # result_text=('GAME OVER','YOU LOST')
                show_result(win_count, count)
                '''answer = messagebox.askyesno('GAME OVER', 'YOU LOST!\nWANT TO PLAY AGAIN?')
                if answer == True:
                    run = True
                    score = 0
                    root.destroy()
                else:
                    run = False
                    root.destroy()'''


    root.mainloop()